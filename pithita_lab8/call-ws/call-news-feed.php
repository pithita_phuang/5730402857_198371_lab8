<?php
        
        $url = simplexml_load_file("http://www.bangkokbiznews.com/rss/feed/technology.xml");
        Header('Content-type: text/xml');
        
        $count = sizeof($url->channel->item);
        
        $title = $sxml->channel->title;
        $i = 0;
        $xml = new SimpleXMLElement('<news/>');
        $xml->addChild('channel_title', $title);

        
        while ($i < $count) {
            $item_title[$i] = $url->channel->item[$i]->title;
            $item_link[$i] = $url->channel->item[$i]->link;

            $item = $xml->addChild('item');
            $item->addChild('title', $item_title[$i]);
            $link = $item->addChild('link', $item_link[$i]);

            $i++;
        }
        
        
        print($xml->asXML());
        ?>