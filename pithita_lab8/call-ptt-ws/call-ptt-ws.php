<?php
header('Content-type:text/xml');
$soap_url = "http://www.pttplc.com/webservice/pttinfo.asmx?op=CurrentOilPrice";

$xml_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <CurrentOilPrice xmlns="http://www.pttplc.com/ptt_webservice/">
      <Language>string</Language>
    </CurrentOilPrice>
  </soap:Body>
</soap:Envelope>';

$header = array(
    'Content-Type: text/xml; charset="utf-8"',
    'Accept: text/xml',
    'Cache-Control: no-cache',
    'Pragma: no-cache',
    'SOAPAction:"http://www.pttplc.com/ptt_webservice/CurrentOilPrice"',
    'Content-lenght:' . strlen($xml_string));


$c = curl_init();
curl_setopt($c, CURLOPT_URL, $soap_url);
curl_setopt($c, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($c, CURLOPT_TIMEOUT, 10);
curl_setopt($c, CURLOPT_POST, TRUE);
curl_setopt($c, CURLOPT_POSTFIELDS, $xml_string);
curl_setopt($c, CURLOPT_HTTPHEADER, $header);

$respond = curl_exec($c);
curl_close($c);

$result = str_replace(strstr($respond, "<soap:Header>"), strstr($respond, "<soap:Body>"), $respond);
echo $result;
?>




